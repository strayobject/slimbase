<?php

return array(
    'app' => array(
        'path_view' => 'app/core/view',
        'debug'     => true,
    ),
    'db' => array(
        'driver'   => '', // mysql
        'host'     => '', // localhost
        'port'     => '', // 3306
        'database' => '',
        'username' => '',
        'password' => '',
    ),
);