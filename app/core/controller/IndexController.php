<?php

namespace core\controller;

use Slim\Slim;

class IndexController
{
    private $app;

    public function indexAction()
    {
        return array('title' => ' - home');
    }

    public function aboutAction()
    {
        return array('title' => ' - about');
    }

    public function formAction()
    {
        return array('title' => ' - form');
    }

    public function saveAction()
    {
        if (!$this->app->request->isPost()) {
            $this->app->flash('error', 'Please fill out the form first.');
            $this->app->redirect('/form');
        }

        return array('title' => ' - home');
    }

    protected function getApp()
    {
        return $this->app;
    }

    public function setApp(Slim $data)
    {
        $this->app = $data;

        return $this;
    }
}