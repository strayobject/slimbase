<?php
session_start();
require 'vendor/autoload.php';

$base_path = realpath(__DIR__.'/..').'/';

if (!is_readable($base_path.'app/core/config/config.php')) {
    die('There is no config.php. Please create it based on the example config_.php');
}

$config = include $base_path.'app/core/config/config.php';
$config['app']['path_base'] = $base_path;

if (!isset($config['app']['debug'])) {
    $config['app']['debug'] = false;
}

if (!isset($config['app']['path_view'])) {
    $config['app']['path_view'] = 'app/core/view';
}

$controller = new core\controller\IndexController();

$app = new \Slim\Slim(array(
    'debug'          => $config['app']['debug'],
    'templates.path' => $base_path.$config['app']['path_view'],
    'view'           => new \Slim\Views\Twig(),
));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => $base_path.'/var/cache'
);
$view->parserExtensions = array(
    new \Slim\Views\TwigExtension(),
);

$app->container->set('config', $config);
$app->container->set('controller', new core\controller\IndexController());

// catch form save request
$app->post('/form/save', function () use ($app) {
    echo $app->container->controller->setApp($app)->saveAction();
});

// catch all requests action
$app->get('/(:action)', function ($action = 'index') use ($app) {

    if (
        !is_readable(
            $app->container->config['app']['path_base']
            .$app->container->config['app']['path_view']
            .'/'.$action.'.html'
        )
    ) {
        throw new Exception(
            'View file for "'.$action.'" is not readable or does not exist in '
            .$app->container->config['app']['path_base']
            .$app->container->config['app']['path_view'].'/',
            404
        );
    }

    $app->render(
        $action.'.html',
        $app->container->controller->setApp($app)->{$action.'Action'}()
    );
});

$app->run();